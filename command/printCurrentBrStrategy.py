from support import CompareOrgCmt
from . import commandStrategy
import subprocess
import plusConfig

class PrintBr(commandStrategy.AbstractCommandStrategy):

    __diff = CompareOrgCmt.AheadOrBehind()

    def command(self):
        return "-b"


    def cmd(self, args):
        all_br = super().printAllBr(args)

        """fetch"""
        _cmd = plusConfig.gitPath + " fetch"
        subprocess.run(_cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True, check=False)

        for br in all_br:
            self.__diff.initiate(br.branchName)
            print(br.plusString(self.__diff))

    def useage(self):
        print(self.command() + "\t打印出所有分支，列表出索引号和描述")
        pass