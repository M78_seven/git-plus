import os

import plusConfig
from . import commandStrategy

class GitCmd(commandStrategy.AbstractCommandStrategy):

    def command(self):
        return "git"

    def cmd(self, args):
        os.system(plusConfig.gitPath + ' %s' % args)

    def useage(self):
        pass