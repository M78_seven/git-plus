import plusConfig
import subprocess

""" 比较出某个分支的 remote 和 本地提对比 """

class AheadOrBehind:

    __ahead = 0
    __behind = 0

    def initiate(self, dist_branch):
        git_path = plusConfig.gitPath

        # declaration
        def execute_cmd(cmd):
            try:
                result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, check=True)
                return result.stdout.decode().strip()
            except subprocess.CalledProcessError as e:
                return None


        # Check if remote branch exists
        is_exist_ref_cmd = f'{git_path} show-ref --verify --quiet refs/remotes/origin/{dist_branch}'
        if execute_cmd(is_exist_ref_cmd) is None:
            return

        # Check behind commits
        cmd_behind = f'{git_path} rev-list {dist_branch}..origin/{dist_branch} --count'
        behind_result = execute_cmd(cmd_behind)
        if behind_result is not None:
            self.__behind = int(behind_result)

        # Check ahead commits
        cmd_ahead = f'{git_path} rev-list origin/{dist_branch}..{dist_branch} --count'
        ahead_result = execute_cmd(cmd_ahead)
        if ahead_result is not None:
            self.__ahead = int(ahead_result)


    """ show different commits counts """
    def showStatus(self):
        if int(self.__behind) > 0:
            return "\033[31m⬇\033[0m"
        if int(self.__ahead) > 0:
            return "\033[33m⬆\033[0m"
        return ""


    """get"""

    def getAheadCount(self):
        return self.__ahead

    def getBehindCount(self):
        return self.__behind



